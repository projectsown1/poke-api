import axios from 'axios';
import { useEffect, useState } from 'react';
import PokeCard from './PokeCard';

const Pokes = () => {

    const [pokes, setPokes] = useState([]);
    const [next,setNext] = useState('');
    const [previous, setPrevious] = useState('');
    const [urlPokes, setUrlPokes] = useState('https://pokeapi.co/api/v2/pokemon-species');
 
    useEffect(()=>{
        const allPokes = async() => {
            try{
                const response = await axios.get(`https://localhost:8443/api/poke`,{
                    params: {
                        url: urlPokes
                    }});
                setPokes(response.data.pokeData);
                setNext(response.data.next);
                setPrevious(response.data.previous);
            }catch(error){
                console.error('Error al obtener la data: ',error);
            }
        }
        allPokes();
    },[urlPokes])

    const updateUrlPreviousPokes = () =>{
        setUrlPokes(previous);
    }

    const updateUrlNextPokes = () =>{
        setUrlPokes(next);
    }

    return (
        <>
            <div 
                className='d-flex justify-content-center flex-column home-background-pokemon' 
                style={{backgroundImage: `url(${process.env.PUBLIC_URL}/img/fondo.jpg)`}}
            >
                <div style={{display:'flex', justifyContent:'center',marginTop:'20px',marginBottom:'20px'}}>
                    <img src={`${process.env.PUBLIC_URL}/img/pokebola.png`} alt="Pokemones" className='home-pokeball-pokemon'/>
                    <img src={`${process.env.PUBLIC_URL}/img/logo-poke.png`} alt="Pokemones" className='home-logo-pokemon'/>
                    <img src={`${process.env.PUBLIC_URL}/img/pokebola.png`} alt="Pokemones" className='home-pokeball-pokemon'/>
                </div>
                <div className='d-flex justify-content-center mb-4'>
                    {
                        previous? 
                            <button 
                                onClick={updateUrlPreviousPokes}
                                className='home-button-pokemon'
                            >
                                Previous
                            </button>
                            :
                            <button 
                                disabled={true}
                                className='home-button-pokemon'
                            >
                                Previous
                            </button>
                    }
                    {
                        next? 
                        <button 
                            onClick={updateUrlNextPokes}
                            className='home-button-pokemon'
                        >
                            Next
                        </button>
                        :
                        <button 
                            disabled={true}
                            className='home-button-pokemon'
                        >
                            Next
                        </button>
                    }
                </div>
                <PokeCard pokes={pokes}/>
            </div>
        </>
    )
}

export default Pokes
