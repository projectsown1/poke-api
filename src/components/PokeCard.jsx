import React from 'react'
import PokeDetails from './PokeDetails'

const PokeCard = ({pokes}) => {
  return (
      <div className='row justify-content-center'>
        { pokes.map((poke) =>(
            <div key={poke.id} className='col-12 col-sm-6 col-md-4 col-lg-3 mb-3 mx-5'>
                <div className='card card-body text-center home-card-pokemon'
                    style={{
                        backgroundImage: `url(${process.env.PUBLIC_URL}/img/fondo-poke.jpg)`,
                    }}>
                    <img src={poke.image} className='img-fluid align-self-center pokedex-img' alt={poke.name}></img>
                    <p>{poke.name}</p>
                    <PokeDetails data={poke} />
                </div>
            </div>
        ))}
    </div>
  )
}

export default PokeCard
