import { Button } from 'primereact/button'
import { Dialog } from 'primereact/dialog';
import React from 'react'
import { usePokesDetails } from '../hooks/usePokesDetails'
import { dialogConfig } from '../utils/dialogConfig';
import {PokeChart} from './PokeChart';

const PokeDetails = ({data}) => {
    const {showDialog, setShowDialog, handleHeader,changeEvolution,changeChart,chart} = usePokesDetails();
 
    return (
        <>
            <Button
                className='rounded bg-none border-0 p-1 mx-1 d-flex justify-content-center'
                onClick={() => setShowDialog(true)}
            >
                Show Detail
            </Button>
            <Dialog
                visible={showDialog}
                pt={dialogConfig}
                onHide={() => setShowDialog(false)}
                header={() => handleHeader() }
                closable={false}
                draggable={false}
            >
                <div className='d-flex w-100 h-100'>
                    <div>
                        <div className='d-flex'>
                            <div className='col-6 d-flex justify-content-around' style={{display:'flex',margin:'15px'}}>                                
                                <div className='pokedex-header-one'></div>
                                <div className='pokedex-header-two'></div>
                                <div className='pokedex-header-three'></div>
                                <div className='pokedex-header-four'></div>
                            </div>                            
                        </div>
                        <div style={{backgroundColor:'#b0b0b0',padding:'20px',margin:'20px', borderRadius:'20px'}}>
                            <div style={{backgroundColor:'white',padding:'20px', borderRadius:'20px'}}>
                                <p className='pokedex-title-pokemon'>
                                    {data.name}
                                </p>
                                <div className='d-flex align-content-center justify-content-center'>
                                    <div className='d-flex align-items-center'>
                                        <img src={data.image} className='pokedex-img-evolution' alt={data.name}></img>
                                    </div>
                                    <div className='m-1 mt-0 mb-0'>
                                        <p className='pokedex-sub-title'>
                                        Stats:
                                        </p>
                                        {
                                            data.stats.map((stat,index)=>(
                                                <div key={index} className='d-flex m-0 p-0'>
                                                    <ul style={{fontWeight:'bold',marginBottom:'0'}}>{stat.stat.name}:</ul>
                                                    <p style={{marginBottom:'0'}}> {stat.base_stat}</p>
                                                </div>
                                            ))
                                        }
                                        <p className='pokedex-sub-title'>
                                        Abilities:
                                        </p>
                                        {
                                            data.abilities.map((ability,index)=>(
                                                    <ul key={index} style={{marginBottom:'0'}}>{ability.ability.name}</ul>
                                            ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className='d-flex'>
                            <div className='col-9 d-flex justify-content-around' style={{display:'flex',margin:'15px'}}>                                
                                <div className='pokedex-footer-one'></div>
                                <div className='pokedex-footer-two' onClick={changeEvolution}>
                                    Evolution
                                </div>
                                <div className='pokedex-footer-three' onClick={changeChart}>
                                    Graph Stats
                                </div>
                            </div>
                        </div>
                        <div className='d-flex'>
                            <div className='col-12 d-flex justify-content-around align-items-center' style={{display:'flex',margin:'15px'}}>
                                <div className='pokedex-footer-four'></div>
                                <div className='pokedex-footer-five'></div>
                            </div>
                        </div>
                    </div>
                    <div 
                        style={{
                            backgroundColor:'black',
                            width:'10px'
                        }}
                    >

                    </div>
                    <div className='pokedex-content-evolution-one'>
                        <div className='pokedex-content-evolution-two'>
                            {
                                (chart)?
                                    <div>
                                        <PokeChart poke={data.stats}/>
                                    </div>
                                :
                                    <div style={{width:'400px'}}>
                                        <p className='pokedex-sub-title'>
                                            Evolutionary line:
                                        </p>
                                        <div className={data.evolution.length < 4 ? 'pokedex-content-evolution-three' : 'pokedex-content-evolution-three-max'}>
                                            {data.evolution.map((evol,index) =>(
                                                <div key={index} className='pokedex-detail-evolution'>
                                                    <img src={evol.image} className='pokedex-img-evolution' alt={evol.name}></img>
                                                    <p className='pokedex-sub-title'>{evol.name}</p>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                            }
                        </div>
                    </div>
                </div>
            </Dialog>
        </>
    )
}

export default PokeDetails
