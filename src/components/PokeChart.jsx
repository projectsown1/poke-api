import React, { useEffect, useState } from 'react';
import { Chart } from 'primereact/chart';
import PropTypes from 'prop-types';

export const PokeChart = ({poke}) => {
    const [chartData, setChartData] = useState({});
    const [chartOptions, setChartOptions] = useState({});

    useEffect(() => {
        let baseData = poke.map(stat => stat.base_stat);
        let labelData = poke.map(stat => stat.stat.name);
        const data = {
            labels: labelData,
            datasets: [{
                label: 'Stats',
                data: baseData,
                fill: true,
                backgroundColor: 'rgba(255, 99, 132, 0.2)',
                borderColor: 'rgb(255, 99, 132)',
                pointBackgroundColor: 'rgb(255, 99, 132)',
                pointBorderColor: '#fff',
                pointHoverBackgroundColor: '#fff',
                pointHoverBorderColor: 'rgb(255, 99, 132)'
            }]
        };

        const options = {
            elements: {
                line: {
                    borderWidth: 3
                }
            },
            plugins: {
                legend: {
                    display: false
                }
            },
            scales: {
                r: {
                    min: 20,
                    max: 140,
                    grid: {
                        color: 'black'
                    },
                    ticks: {
                        stepSize: 10,
                        color: 'black',
                    },
                    angleLines: {
                        color: 'black'
                    },
                    pointLabels: {
                        color: 'black',
                        font: {
                            size: 14,
                            weight:'bold'
                        }
                    }
                }
            }
        };

        setChartData(data);
        setChartOptions(options);
    }, [poke]);

    return (
        <div>
            <Chart type="radar" data={chartData} options={chartOptions} width='500px' style={{marginLeft:'-65px',marginRight:'-35px'}}/>
        </div>
    );
}

PokeChart.propTypes = {
    poke: PropTypes.array.isRequired,
};
