import React, { useState } from 'react'

const usePokesDetails = () => {

    const [showDialog, setShowDialog] = useState(false);
    const [chart,setChart] = useState(false);

    const changeEvolution = () => {
        setChart(false)
    }

    const changeChart = () => {
        setChart(true)
    }

    const handleClose = () => {
        setShowDialog(false);
        changeEvolution();
    }

    const handleHeader = () => {
        return (
            <>
                <div style={{
                    borderRadius:'20px',
                    flexDirection:'column',
                    display:'flex'
                    }}>
                    <button 
                        onClick={handleClose}
                        className='pokedex-img-close'
                    >
                        <img src={`${process.env.PUBLIC_URL}/img/close.svg`} alt='close'></img>
                    </button>
                </div>
            </>
        );
    }

    return {
        showDialog,
        setShowDialog,
        handleHeader,
        handleClose,
        changeEvolution,
        changeChart,
        chart
    }
}

export {usePokesDetails}
