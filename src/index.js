import React from 'react';
import ReactDOM from 'react-dom/client';
import './styles/app.scss';
import reportWebVitals from './reportWebVitals';
import Pokes from './components/Pokes';

import 'bootstrap/dist/css/bootstrap.min.css'


const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
      <Pokes />
  </React.StrictMode>
);

reportWebVitals();
